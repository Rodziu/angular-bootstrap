/*
 * Twitter Bootstrap plugin for AngularJS.
 * Copyright (c) 2016-2018 Rodziu <mateusz.rohde@gmail.com>
 * License: MIT
 */
angular.module('angularBS', [
	'angularBS.modal', 'angularBS.dropdown', 'angularBS.tooltip', 'angularBS.popover', 'angularBS.collapse',
	'angularBS.carousel'
]);