# Twitter Bootstrap plugin for AngularJS

It's a lightweight implementation of [Bootstrap JavaScript plugins](https://getbootstrap.com/docs/3.3/javascript/) in pure AngularJS.

[Live Demo](https://mateuszrohde.pl/repository/angular-bootstrap/demo/index.html)

## Prerequisites

- AngularJS,
- Twitter Bootstrap.

## Installation

```
yarn add angularjs-bootstrap
yarn install
```

## Features

Bootstrap Modal, Dropdown, Tooltip, Popover, Collapse, Carousel plugins implemented in angular-way (see more at [Live Demo](https://mateuszrohde.pl/repository/angular-bootstrap/demo/index.html)).

Scrollspy, Tab, Alert, Button & Affix plugins are not implemented because of better ways to do it in pure AngularJS and/or CSS.